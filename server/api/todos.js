require('../config/pg.config');

app.get('/', (req, res) => {
    client.query('SELECT * FROM todos', (error, response) => {
        if (error) {
            throw error;
        } else {
            res.send(response.rows);
        }
    });
    console.log("GET Request successful!");
});

app.post('/', (req, res) => {
    try {
        let todo = {
            "todoDescription": req.body.todoDescription,
            "createdAt": req.body.createdAt,
            "isDone": req.body.isDone
        }
        let insertQuery = {
            text: "INSERT INTO todos(todo_desc, todo_date, is_done) VALUES ($1, $2, $3) RETURNING *",
            values: [todo.todoDescription, todo.createdAt, todo.isDone]
        }
        client.query(insertQuery, (error) => {
            if (error) {
                throw error;
            }
        });
        client.query("SELECT * FROM todos", (error, response) => {
            if (error) {
                throw error;
            } else {
                res.send(response.rows);
            }
        });
        console.log("POST Request successful!");
    } catch (error) {
        console.error(error);
    }
});

app.delete('/del/:id', (req, res) => {
    let deleteQuery = {
        text: "DELETE FROM todos WHERE id=($1) RETURNING *;",
        values: [req.params.id]
    };
    console.log(deleteQuery);
    client.query(deleteQuery, (error) => {
        if (error) {
            console.error(error);
        }
    });
    res.send("DELETE Request successful!");
});

app.put('/edit/:id', (req, res) => {
    try {
        console.log(req.body);
        var todo = {
            "todoId": req.body.id,
            "todoDescription": req.body.todo_desc,
            "createdAt": req.body.todo_date,
            "isDone": req.body.is_done
        }
        let updateQuery = {
            text: "UPDATE todos SET todo_desc=($1), todo_date=($2), is_done=($3) WHERE id=($4) RETURNING *",
            values: [todo.todoDescription, todo.createdAt, todo.isDone, todo.todoId]
        }
        client.query(updateQuery, (error, response) => {
            if (error) {
                throw error;
            }
        });
        res.send("UPDATE Request completed!");
        console.log("UPDATE Request completed!");
    } catch (error) {
        console.error(error);
    }
});


