global.express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');

global.app = express();

app.use(express.static(__dirname + "/client/"))
app.use(bodyParser.json());
app.use(
    bodyParser.urlencoded({
        extended: true
    })
)
app.use(cors());

const port = process.env.PORT || 3000;

app.listen(port, () => console.log(`Server started at ${port}`));

const todos = require('./api/todos');