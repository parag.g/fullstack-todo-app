var express = require('express');
var app = express();

var port = process.env.PORT || 8080;

app.use(express.static(__dirname));

app.get('/', (req, res) => {
    res.send('index');
})

app.listen(port, () => {
    console.log(`App started at port ${port}`);
});